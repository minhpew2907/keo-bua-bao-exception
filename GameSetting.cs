﻿

namespace TroChoiKeoBuaBaoException
{
    public class GameSetting
    {
        public int KeoBuaBaoLuatLe(int player1, int player2)
        {
            if (player1 == (int)KeoBuaBaoEnum.KEO && player2 == (int)KeoBuaBaoEnum.BAO
                || player1 == (int)KeoBuaBaoEnum.BUA && player2 == (int)KeoBuaBaoEnum.KEO
                || player1 == (int)KeoBuaBaoEnum.BAO && player2 == (int)KeoBuaBaoEnum.BUA)
            {
                return (int)KeoBuaBaoEnum.PLAYER_1_THANG;
            }
            if (player2 == (int)KeoBuaBaoEnum.KEO && player1 == (int)KeoBuaBaoEnum.BAO
                || player2 == (int)KeoBuaBaoEnum.BUA && player1 == (int)KeoBuaBaoEnum.KEO
                || player2 == (int)KeoBuaBaoEnum.BAO && player1 == (int)KeoBuaBaoEnum.BUA)
            {
                return (int)KeoBuaBaoEnum.PLAYER_2_THANG;
            }
            return (int)KeoBuaBaoEnum.HOA;
        }

        public int KetQuaTranDau(int result)
        {
            if (result == (int)KeoBuaBaoEnum.HOA)
            {
                Console.WriteLine("Hoa nhau");
            }
            else if (result == (int)KeoBuaBaoEnum.PLAYER_1_THANG)
            {
                Console.WriteLine("Nguoi choi 1 thang");
            }
            else
            {
                Console.WriteLine("Nguoi choi 2 thang");
            }
            return result;
        }

        public void Choi()
        {
            LuaChon();
            int player1Choose = 0;
            int player2Choose = 0;

            Console.WriteLine("Nguoi choi 1 chon: ");
            player1Choose = int.Parse(Console.ReadLine());
            if (player1Choose > 3 || player1Choose < 1)
            {
                throw new ArgumentException("Chon cac lua chon bang tren.");
            }

            Console.WriteLine("Nguoi choi 2 chon: ");
            player2Choose = int.Parse(Console.ReadLine());
            if (player2Choose > 3 || player2Choose < 1)
            {
                throw new ArgumentException("Chon cac lua chon bang tren.");
            }
            int kiemTraKetQua = KeoBuaBaoLuatLe(player1Choose, player2Choose);
            Console.WriteLine(KetQuaTranDau(kiemTraKetQua));
        }

        public void LuaChon()
        {
            Console.WriteLine("1. KEO");
            Console.WriteLine("2. BUA");
            Console.WriteLine("3. BAO");
        }

        public void GameMenu()
        {
            Console.WriteLine(" --------Game Keo Bua Bao------");
            Console.WriteLine("| 1. Choi.                     |");
            Console.WriteLine("| 2. Thoat.                    |");
            Console.WriteLine(" ------------------------------");
        }
    }
}
